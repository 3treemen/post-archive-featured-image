<?php
/**
 * Plugin Name:     Post Archive Featured Image
 * Plugin URI:      zoo.nl
 * Description:     Show Featured Image on Post Archive page
 * Author:          Ronnie Stevens
 * Author URI:      https://zoo.nl
 * Text Domain:     PAFeaturedImage
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         PAFeaturedImage
 */

// Your code starts here.

function get_featured_image_post_archive() {

    if (is_home() && get_option('page_for_posts')) {
        $img_url = get_the_post_thumbnail_url(get_option('page_for_posts'));
        echo "
            <style type='text/css'>
                #hero-section {
                    background-image: url('$img_url');
                }
            </style>
                ";
    }
}
add_action( 'wp_head', 'get_featured_image_post_archive' );
