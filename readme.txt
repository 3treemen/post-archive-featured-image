=== PAFeaturedImage ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: https://example.com/
Tags: Genesis, Featured Image
Requires at least: 4.5
Tested up to: 5.4.1
Requires PHP: 5.6
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Show the featured image on the post archive page of the Corporate Pro theme.

== Description ==

For the Corporate Pro theme of Genesis I needed to show the Featured Image of the static page that is set to show the Post Archive.
This plugin gets the url of that Featured Image, and inserts it on the ID hero-section.

Default it shows the Featured Image of the Homepage. This one is also set in an inline css, but on the class hero-section.


== Installation ==

1. Upload `PAFeaturedImage` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress


== Changelog ==

= 1.0 =
* Inital wp scaffold.
* Insert css code in header

